#![allow(unused_variables)]
use actix::prelude::*;
use actix_web::server::HttpServer;
use actix_web::{fs, http, ws, App, Error, HttpRequest, HttpResponse};
use rand::{self, rngs::ThreadRng, Rng};
use std::{
    collections::HashMap,
    io::{stdin, BufRead, BufReader},
    sync::mpsc::channel,
    thread,
};

#[derive(Message)]
pub struct Message(pub String);

#[derive(Message)]
#[rtype(usize)]
pub struct Connect {
    pub addr: Recipient<Message>,
}

#[derive(Message)]
pub struct Disconnect {
    pub id: usize,
}

pub struct PlotkaServer {
    sessions: HashMap<usize, Recipient<Message>>,
    rng: ThreadRng,
}

impl Default for PlotkaServer {
    fn default() -> PlotkaServer {
        PlotkaServer {
            sessions: HashMap::new(),
            rng: rand::thread_rng(),
        }
    }
}

impl PlotkaServer {
    fn send_message(&self, message: &str, skip_id: Option<usize>) {
        if let Some(skip_id) = skip_id {
            for id in self.sessions.keys() {
                if *id != skip_id {
                    if let Some(addr) = self.sessions.get(id) {
                        let _ = addr.do_send(Message(message.to_owned()));
                    }
                }
            }
        } else {
            for id in self.sessions.keys() {
                if let Some(addr) = self.sessions.get(id) {
                    let _ = addr.do_send(Message(message.to_owned()));
                }
            }
        }
    }
}

impl Actor for PlotkaServer {
    type Context = Context<Self>;
}

impl Handler<Connect> for PlotkaServer {
    type Result = usize;

    fn handle(&mut self, msg: Connect, _: &mut Context<Self>) -> Self::Result {
        println!("Someone joined");
        self.send_message("Someone joined", Some(0));

        let id = self.rng.gen::<usize>();
        self.sessions.insert(id, msg.addr);

        id
    }
}

impl Handler<Disconnect> for PlotkaServer {
    type Result = ();

    fn handle(&mut self, msg: Disconnect, _: &mut Context<Self>) {
        println!("Someone disconnected");

        if self.sessions.remove(&msg.id).is_some() {}
    }
}

impl Handler<Message> for PlotkaServer {
    type Result = ();

    fn handle(&mut self, msg: Message, _: &mut Context<Self>) {
        self.send_message(&msg.0, None);
    }
}

struct WsSessionState {
    addr: Addr<PlotkaServer>,
}

fn chat_route(
    req: &HttpRequest<WsSessionState>,
) -> Result<HttpResponse, Error> {
    ws::start(req, WsSession { id: 0 })
}

struct WsSession {
    id: usize,
}

impl StreamHandler<ws::Message, ws::ProtocolError> for WsSession {
    fn handle(&mut self, _msg: ws::Message, _ctx: &mut Self::Context) {}
}

impl Actor for WsSession {
    type Context = ws::WebsocketContext<Self, WsSessionState>;

    fn started(&mut self, ctx: &mut Self::Context) {
        let addr = ctx.address();

        ctx.state()
            .addr
            .send(Connect {
                addr: addr.recipient(),
            })
            .into_actor(self)
            .then(|res, act, ctx| {
                match res {
                    Ok(res) => act.id = res,
                    _ => ctx.stop(),
                }
                fut::ok(())
            })
            .wait(ctx);
    }

    fn stopping(&mut self, ctx: &mut Self::Context) -> Running {
        ctx.state().addr.do_send(Disconnect { id: self.id });

        Running::Stop
    }
}

impl Handler<Message> for WsSession {
    type Result = ();

    fn handle(&mut self, msg: Message, ctx: &mut Self::Context) {
        ctx.text(msg.0);
    }
}

fn main() {
    pretty_env_logger::init();

    let (tx, rx) = channel();

    let handle = thread::spawn(move || {
        let sys = actix::System::new("plotka");

        let server = Arbiter::start(|_| PlotkaServer::default());

        {
            let server = server.clone();

            HttpServer::new(move || {
                let state = WsSessionState {
                    addr: server.clone(),
                };

                App::with_state(state)
                    .resource("/", |r| {
                        r.method(http::Method::GET).f(|_| {
                            HttpResponse::Found()
                                .header("LOCATION", "/static/index.html")
                                .finish()
                        })
                    })
                    .resource("/ws/", |r| r.route().f(chat_route))
                    .handler(
                        "/static/",
                        fs::StaticFiles::new("static/").unwrap(),
                    )
            })
            .bind("127.0.0.1:8080")
            .unwrap()
            .start();
        }

        println!("Started http server: 127.0.0.1:8080");

        tx.send(server).unwrap();

        let _ = sys.run();
    });

    let addr = rx.recv().unwrap();

    let stdio_handle = thread::spawn(move || {
        BufReader::new(stdin())
            .lines()
            .filter(|line| line.is_ok())
            .for_each(|line| {
                addr.do_send(Message(line.unwrap()));
            });
    });

    let _ = handle.join();
}
